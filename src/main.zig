const std = @import("std");

var mode = Mode.Normal;
var command_buffer: [1024]u8 = undefined;
var command_len: usize = 0;

const termios = @cImport({
    @cInclude("termios.h");
    @cInclude("unistd.h");
});

var original_termios: termios.struct_termios = undefined;

const Mode = enum {
    Normal,
    Insert,
    Command,
};

/// Enable raw mode for the terminal
fn enableRawMode() !void {
    // Get current terminal attributes
    if (termios.tcgetattr(termios.STDIN_FILENO, &original_termios) != 0) {
        std.debug.print("Failed to get terminal attributes\n", .{});
        return;
    }

    // Create a copy of the original terminal attributes
    var raw = original_termios;

    // Modify flags for raw mode
    raw.c_lflag &= termios.ECHO - 1;
    raw.c_lflag &= termios.ICANON - 1;
    raw.c_lflag &= termios.ISIG - 1;
    raw.c_lflag &= termios.IEXTEN - 1;

    // Set terminal attributes
    if (termios.tcsetattr(termios.STDIN_FILENO, termios.TCSAFLUSH, &raw) != 0) {
        std.debug.print("Failed to set terminal attributes\n", .{});
        return;
    }
}

// Disable raw mode and restore original terminal attributes
fn disableRawMode() !void {
    if (termios.tcsetattr(termios.STDIN_FILENO, termios.TCSAFLUSH, &original_termios) != 0) {
        std.debug.print("Failed to set terminal attributes\n", .{});
        return;
    }
}

// Handle input in Normal mode
fn handle_normal_mode(buffer: u8, stdout: *std.io.Writer) void {
    switch (buffer) {
        'i' => {
            mode = .Insert;
        },
        ':' => {
            mode = .Command;
            command_len = 0;
            stdout.print(": ", .{}) catch {};
        },
        'q' => {
            std.process.exit(0);
        },
        else => {},
    }
}

// Handle input in Insert mode
fn handle_insert_mode(buffer: u8) void {
    if (buffer == 0x1B) { // ESC key
        mode = .Normal; // Switch to Normal mode
    } else {
        // Insert the character into the document
        std.debug.print("{c}", .{buffer});
    }
}

// Handle input in Command mode
fn handle_command_mode(buffer: u8, stdout: *std.io.Writer) void {
    if (buffer == '\n') { //Enter key
        mode = .Normal;
        stdout.print("\nExecuting command: {s}\n", .{std.mem.sliceAsBytes(&command_buffer)[0..command_len]}) catch {};
        // Parse and execute the command
        command_len = 0; // Reset the command buffer
    } else if (command_len < command_buffer.len) {
        command_buffer[command_len] = buffer;
        command_len += 1;
    }
}

// Main function
pub fn main() !void {

    // Enable raw mode
    // This will disable canonical mode and echo mode
    // Canonical mode means that input is line-buffered
    const stdout = std.io.getStdOut().writer();
    try stdout.print("Welcome to Vized\n", .{});

    var stdin = std.io.getStdIn().reader();
    var buffer: [1]u8 = undefined;
    try enableRawMode();
    defer {
        _ = disableRawMode() catch |err| {
            std.debug.print("Failed to disable raw mode: {s}\n", .{err});
        };
    }

    // Main application loop
    while (true) {
        const bytesRead = try stdin.read(&buffer);
        if (bytesRead == 0) break;

        switch (mode) {
            .Normal => handle_normal_mode(buffer[0], &stdout),
            .Insert => handle_insert_mode(buffer[0], &stdout),
            .Command => handle_command_mode(buffer[0], &stdout),
        }
    }
}
