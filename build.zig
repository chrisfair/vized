const std = @import("std");

pub fn build(b: *std.Build) void {
    const exe = b.addExecutable(.{
        .name = "vized",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = b.host,
    });

    exe.linkLibC();
    b.installArtifact(exe);

    const run_exe = b.addRunArtifact(exe);

    const run_step = b.step("run", "Run the executable");
    run_step.dependOn(&run_exe.step);
}
